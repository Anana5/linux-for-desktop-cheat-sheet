# Bash

## Key shortcuts

### Moving around
| Key combination | Action |
| --------------- | ------ |
| ctrl + f | Move forward one character |
| ctrl + b | Move backward one character |
| ctrl + p | Previous command |
| ctrl + n | Next command |
| ctrl + a | Move to the beginning of the current line |
| ctrl + e | Move to the end of the current line |
| alt + f  | Move forward one word |
| alt + b  | Move backward one word |

### Manipulating the input
| Key combination | Action |
| --------------- | ------ |
| ctrl + l | Clear the screen |
| ctrl + k | Cut the text after the cursor (remove the text and place it in a buffer) |
| ctrl + u | Cut the text before the cursor |
| ctrl + y | Yank the text (paste the text from a buffer) |
| ctrl + w | Cut the text from cursor until previous whitespace |
| alt + d  | Cut the text from cursor until the end of a word |
| alt + backspace | Cut text from cursor until beginning of a word |
| alt + . | Insert last argument of the previous command |

### Searching
| Key combination | Action |
| --------------- | ------ |
| ctrl + r | Reverse search |
| ctrl + j | Stop searching at currently selected entry |
| ctrl + g | Cancel the search |

### Process control
| Key combination | Action |
| --------------- | ------ |
| ctrl + c | Kill/Interrupt the running process (send SIGINT) |
| ctrl + z | Suspend the running process (send SIGTSTP) |
| ctrl + s | Stop outputting text to the screen |
| ctrl + q | Resume outputting to the screen |

## Special aliases

| Command | Action |
| --------------- | ------ |
| !! | Repeat previous command |
| !$ | Repeat the last argument ot the previous command |
| !n | Repeat n-th argument from the previous command |
| !* | Repeat all arguments of the previous command |
