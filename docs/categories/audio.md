# Audio

## Pulse audio

### Troubleshooting

Listing available inputs:
`pacmd list-sources`

Listing available outputs:
`pacmd list-sinks`

Restarting pulseaudio server:  
`pulseaudio -k` - kills the server  
`pulseaudio -D` - starts and daemonizes the server  

### Null sink/source

Null sinks and sources are sometimes useful for OBS recording.
They can be created with the pacmd command.

Loading null sink module:  
`pacmd load-module module-null-sink sink_name=MySink`

Loading null source module:  
`pacmd load-module module-null-source`

Changing properties:  
`pacmd update-sink-proplist MySink device.description=MySink`

